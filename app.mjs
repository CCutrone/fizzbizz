import express from 'express';
import route from './routes.js';
import fizzbuzz from './modules/crypto_fizzbuzz.mjs'

express()
    .use(express.static(route.__dirname + '/public'))
    .use(express.json())
    .get('/api/fizzbuzz/:max', fizzbuzz.fizzbuzz)
    .listen(8000)