import crypto from 'crypto';

/**
 * Determines if a number is a prime or not. In order to see if a number is 
 * a prime, only prime numbers up to it's square root need to be checked against
 * it (every number is either a prime, or can be created the combination of 2 and/or 
 * prime numbers). 
 * @param { Number } num The number that needs to be checked for primality
 * @param { Array<Number> } previousPrimes All prime numbers previously calculated
 */
const isPrime = (num, previousPrimes) => {
    if(num == 1 || num == 2) return true; //don't add 1 or 2 to the primes array, it will mess with the check

    if(num % 2 == 0) return false;

    const sqrt = Math.sqrt(num);
    let index = 0;

    while(index < previousPrimes.length && previousPrimes[index] <= sqrt){
        if(num % previousPrimes[index++] == 0) return false;
        //the number is evenly divisible by a previous prime number and therefore is not a prime.
    }
    previousPrimes.push(num);

    return true;
}

const routes = {
    fizzbuzz(req, resp){
        const maxPrime = req.params.max;
        const primes = []; //for use with the isPrime function
        const hexes = []; //all the hexes of the fizzbuzz strings
        let fizzbuzz = i;

        for(let i = 1; i <= maxPrime; i++) {
            //if the number is a prime add 'fizz' to the end of it, otherwise add 'buzz' to the end of it
            fizzbuzz = i + isPrime(i, primes)? 'fizz' : 'buzz';
    
            const hash = crypto.createHash('sha1').update(fizzbuzz).digest('hex');

            hexes.push({ fizzbuzz, hash });
        }
        resp.json(hexes);
    }
}

export { routes };
export default routes;